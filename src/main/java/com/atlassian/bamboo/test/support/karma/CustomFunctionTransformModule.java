package com.atlassian.bamboo.test.support.karma;

import com.atlassian.maven.plugins.soytohtml.core.AuiPluginModule;
import com.atlassian.soy.impl.functions.ConcatFunction;

import com.google.inject.multibindings.Multibinder;
import com.google.template.soy.shared.restricted.SoyFunction;

public class CustomFunctionTransformModule extends AuiPluginModule
{

    public CustomFunctionTransformModule(final String propertiesFileLocation)
    {
        super(propertiesFileLocation);
    }

    @Override
    protected void bindFunctions(final Multibinder<SoyFunction> binder)
    {
        super.bindFunctions(binder);
        binder.addBinding().to(ContextPathFunction.class);
    }
}
